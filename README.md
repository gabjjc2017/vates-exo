# Angular Material CRUD Forms

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Live Demos

Hosted in Azure.

- [Angular Material CRUD Forms](https://angular-material-crud-forms.azurewebsites.net/)

## Tutorials

Tutorial series about this project can be found [here](https://www.cc28tech.com/angular-material-crud-forms-part-1/). 

## Getting Started

1. Clone this repository

   ```bash
   git clone https://github.com/cwun/angular-material-crud-forms.git
   cd angular-material-crud-forms
   ```

1. Install the npm packages

   ```bash
   npm install
   ```
   
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).



## Actualización  -- 2020 --
Para que funcione en la actualidad es necesario realizar las siguientes instalaciones 

Run 'npm install node-sass'

En caso de tener una instalación previa de Angular que sea superior a la version 6.0.8 se debe realizar un downgrade 
```
npm uninstall -g @angular/cli
npm cache clean
npm install -g @angular/cli@6.0.8
```