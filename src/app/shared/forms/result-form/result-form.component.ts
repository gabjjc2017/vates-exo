import { ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import {
  ApexAxisChartSeries,
  ApexTitleSubtitle,
  ApexChart,
  ApexXAxis,
  ChartComponent
} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  title: ApexTitleSubtitle;
  xaxis: ApexXAxis;
};

@Component ({
    selector:     'app-result-form',
    templateUrl: './result-form.component.html',
    styleUrls: ['./result-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ResultFormComponent {
    // Receive formData object from the parent 'NewContactComponent'
    @Input() formData: any;

  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;



  constructor() {
    
    let PURPOSE = Math.floor(Math.random() * (99 -10)) + 100;
    let PEOPLE= Math.floor(Math.random() * (99 -10)) + 100;
    let CUSTOMER= Math.floor(Math.random() * (99 -10)) + 100;
    let ABUNDANCE= Math.floor(Math.random() * (99 -10)) + 100;
    let SUSTENTABILITY= Math.floor(Math.random() * (99 -10)) + 100;
    let PROCESSES= Math.floor(Math.random() * (99 -10)) + 100;
    let PRODUCT= Math.floor(Math.random() * (99 -10)) + 100;
    let METRICS= Math.floor(Math.random() * (99 -10)) + 100;

    this.chartOptions = {
      series: [
        {
          name: "Series 1",
          data: [PURPOSE, PEOPLE, CUSTOMER, ABUNDANCE,SUSTENTABILITY, PROCESSES, PRODUCT, METRICS]
        }
      ],
      chart: {
        height: 500,
        type: "radar",
        width: 500
      },
      title: {
        text: "EQL Score"
      },
      xaxis: {
        categories: [
          "PURPOSE",
          "PEOPLE",
          "CUSTOMER",
          "ABUNDANCE",
          "SUSTENTABILITY (BUSINESS MODEL)",
          "PROCESSES",
          "PRODUCT",
          "METRICS"]
      }
    };
  }

}
